<?php

/**
 * @file
 * Core layout for every page
 */
 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body>
    <!-- Layout -->
    <div id="container" style="width: <?php print pagewidth() . widthtype(); ?>;">
      <div id="container-inner">
        <!-- Header -->
        <div id="header">
          <?php print $menu; ?>
          <?php print $logo; ?>
          <?php print $header; ?>
          <?php if ($search_box) { ?>
            <div class="block block-theme">
              <?php print $search_box ?>
            </div>
          <?php } ?>
        </div>
        <?php if ($secondary_links or $subheader) { ?>   
          <div id="subheader">
          <?php if (isset($secondary_links)) : ?>
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
            <?php print $subheader; ?>
          <?php endif; ?>
        <?php } ?>
        </div>
        <!-- Content -->
        <div id="content">
          <?php print $above_content  ?>
          <?php if ($show_messages) { print $messages; } ?>
          <?php print $help_text ?>
          <?php if ($left) { ?>
            <div id="sidebar-left" class="sidebar">
            <?php print $left ?>
            </div>
          <?php } ?>
          <?php if ($right) { ?>
            <div id="sidebar-right" class="sidebar">
                <?php print $right ?>
            </div>
          <?php } ?>
          <?php print_r($var_show_blocks); ?>
          <div id="center" <?php print phptemplate_body_class($left, $right); ?>>
             <?php print $top_content ?>
             <h1><?php print $title ?></h1>
             <?php if ($tabs) { print $tabs; } ?>
             <?php if ($tabs2) { print $tabs2; } ?>
             <?php print $content; ?>
          </div>
        </div>
        <br id="end-content" />
      </div>
      <!-- Footer -->
      <div id="footer">
        <?php print $footer; ?>
        <br id="end-content" />
      </div>
      <!-- /#footer -->
      <?php print $closure ?>
  </body>
</html>
