<?php

/**
 * @file
 * Core functions for the operations of the theme
 */

/**
 * Index
 * 1. Theme Settings
 * 2. PHP Template Functions
 * 3. UD Functions
 */

/**
 * 1. Theme Settings
 */

if (is_null(theme_get_setting('wrapper_width'))) {
  global $theme_key;

  $defaults = array(
    'wrapper_width' => 1000,
    'width_type' => '0',
    'display_help' => '1',
  );

  $settings = theme_get_settings($theme_key);

  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }

  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );

  theme_get_setting('', TRUE);
}

/**
 * 2. PHP Template Functions
 */

/**
 * hook_body_class()
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($left, $right) {
  if ($left != '' && $right != '') {
    $class = 'sidebars';
  }
  else {
    if ($left != '') {
      $class = 'sidebar-left';
    }
    if ($right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * hook _preprocess_page
 * Generate / override exsisting variables
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['menu']        = udtheme_whichmenu($vars['primary_links']);
  $vars['logo']        = udtheme_buildlogo($vars);
  $vars['footer']      = udtheme_footer($vars['footer']);
  $vars['help_text']   = udtheme_help($vars['help']);
}

/**
 * 3. UD Functions
 */

/**
 * Automatically use the nice_menu module if available
 * If not available, no drop downs will be displayed
 */
function udtheme_whichmenu($primary_links) {
  if (module_exists('nice_menus')) {
    // Nice Menu
    $menu = theme('nice_menu_primary_links', $primary_links);
  }
  else {
    // Standard Menu
    $menu = theme('links', $primary_links);
  }

  if (isset($menu)) {
    return $menu;
  }
}

/**
 * Generate a logo if the variable exists
 */
function udtheme_buildlogo($vars) {
  if ($vars['logo']) {
    $logo = '<div id="logo">' .
            '  <a href="' . check_url($vars['front_page']) . '" title="' . $vars['site_title'] . '">' .
            '    <img src="' . check_url($vars['logo']) . '" alt="' . $vars['site_title'] . '" />' .
            '  </a>' .
            '</div>';
  }
  if (isset($logo)) {
    return $logo;
  }
}

/**
 * Generate a or use the default if one has not been set
 */
function udtheme_footer($footmsg) {
  if (empty($footmsg)) {
  $footmsg = t('<div class="block"><h2>@legal</h2> !copy </div>'
               . '<div class="block"><h2>@more</h2>!getubuntu !brainstorm !forums !spread </div>',
               array(
                 '@legal' => t('Legal Disclaimer'),
                 '!copy' => t('&copy; 2010 Canonical Ltd. Ubuntu and Canonical are registered trademarks of Canonical Ltd.'),
                 '@more' => t('More Ubuntu'),
                 '!getubuntu' => l(t('Get Ubuntu'), 'http://ubuntu.com/getubuntu/download/'),
                 '!brainstorm' => l(t('Ubuntu Brainstorm'), 'http://brainstorm.ubuntu.com/'),
                 '!forums' => l(t('Ubuntu Forums'), 'http://ubuntuforums.org/'),
                 '!spread' => l(t('Spread Ubuntu'), 'http://spreadubuntu.neomenlo.org/'),
               )
             );
  }

  return $footmsg;
}


//warning: Missing argument 1 for udtheme_help() without = NULL
function udtheme_help($help = NULL) {
  switch (theme_get_setting('display_help', 1)) {
    case 0:
      return NULL;
      break;
    case 1:
      return $help;
      break;
  }
}

/**
 * Code to handle things needing a static width
 */
function pagewidth($subtract = 0) {
  return theme_get_setting('wrapper_width', 1000) - $subtract;
}

function widthtype() {
  switch (theme_get_setting('width_type', 0)) {
    case 0:
      return 'px';
      break;
    case 1:
      return '%';
      break;
  }
}

