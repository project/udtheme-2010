<?php

/**
 * @file
 * Additional theme settings the user can alter
 */

function phptemplate_settings($saved_settings) {

  // Set default variables
  $defaults = array(
    'wrapper_width' => 1000,
    'width_type' => '0',
    'display_help' => '1',
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
  );
  $form['page']['wrapper_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Width (default=1000px)'),
    '#default_value' => $settings['wrapper_width'],
  );
  $form['page']['width_type'] = array(
    '#type' => 'radios',
    '#title' => t('Width Type'),
    '#options' => array(
      '0' => t('px'),
      '1' => t('%'),
    ),
  );
  $form['page']['display_help'] = array(
    '#type' => 'radios',
    '#title' => t('Display Help'),
    '#options' => array(
      '1' => t('Yes'),
      '0' => t('No'),
    ),
    '#description' => t('Display help above the content or not.'),
    '#default_value' => $settings['display_help'],
  );

  // Return the additional form widgets
  return $form;
}
